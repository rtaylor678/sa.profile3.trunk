﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Sa.Profile.Profile3.Business.Scrambler
{
    public class DataTableExtentions //ttodo: change to static
    {
        public IEnumerable<T> AsEnumerable2<T>(DataTable table) where T : new()
        {
            if (table == null)
            {
                throw new NullReferenceException("DataTable");
            }
            int propertiesLength = typeof(T).GetProperties().Length;
            if (propertiesLength == 0)
            {
                throw new NullReferenceException("Properties");
            }
            var objList = new List<T>();

            foreach (DataRow row in table.Rows)
            {
                var obj = new T();
                System.Reflection.PropertyInfo[] objProperties = obj.GetType().GetProperties();
                for (int i = 0; i < propertiesLength; i++)
                {
                    System.Reflection.PropertyInfo property = objProperties[i];
                    if (table.Columns.Contains(property.Name))
                    {
                        object objValue = row[property.Name];
                        var propertyType = property.PropertyType;
                        if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            propertyType = propertyType.GetGenericArguments()[0];
                        }
                        if (objValue != DBNull.Value)
                        {
                            objProperties[i].SetValue(obj, Convert.ChangeType(objValue, propertyType, System.Globalization.CultureInfo.CurrentCulture), null);
                        }
                    }
                }
                objList.Add(obj);
            }
            return objList;
        }
    }
}
