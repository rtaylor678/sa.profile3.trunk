using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    public class Per
    {
        public decimal? AbsHrs { get; set; }
        
        public decimal? Contract { get; set; }
        
        public decimal? GA { get; set; }
        
        public decimal? NumPers { get; set; }
        
        public decimal? OVTHours { get; set; }
        
        public decimal? OVTPcnt { get; set; }
        
        public string PersID { get; set; }
        
        public decimal? STH { get; set; }
        
        public int SubmissionID { get; set; }
    }
}
