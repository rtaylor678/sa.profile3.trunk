using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Submission
    {
        public DateTime? Date { get; set; }
        
        public string RefID { get; set; }
        
        public string RptCurrency { get; set; }
        
        public int SubmissionID { get; set; }
        
        public string UOM { get; set; }
    }
}
