using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class MaintTA
    {
        public DateTime? PrevTADate { get; set; }
        
        public string ProcessID { get; set; }
        
        public int SubmissionID { get; set; }
        
        public decimal? TAContMPS { get; set; }
        
        public decimal? TAContOCC { get; set; }
        
        public decimal? TACostLocal { get; set; }
        
        public decimal? TACptlLocal { get; set; }
        
        public DateTime? TADate { get; set; }
        
        public int? TAExceptions { get; set; }
        
        public decimal? TAExpLocal { get; set; }
        
        public decimal? TAHrsDown { get; set; }
        
        public decimal? TALaborCostLocal { get; set; }
        
        public decimal? TAMPSOVTPcnt { get; set; }
        
        public decimal? TAMPSSTH { get; set; }
        
        public decimal? TAOCCOVT { get; set; }
        
        public decimal? TAOCCSTH { get; set; }
        
        public decimal? TAOvhdLocal { get; set; }
        
        public int UnitID { get; set; }
    }
}
