using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Absence
    {
        public string CategoryID { get; set; }
        
        public decimal? MPSAbs { get; set; }
        
        public decimal? OCCAbs { get; set; }
        
        public int SubmissionID { get; set; }
    }
}
