using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class FiredHeater
    {
        public decimal? AbsorbedDuty { get; set; }

        public decimal? CombAirTemp { get; set; }

        public decimal? FiredDuty { get; set; }
        
        public string FuelType { get; set; }

        public decimal? FurnInTemp { get; set; }

        public decimal? FurnOutTemp { get; set; }
        
        public string HeaterName { get; set; }
        
        public int HeaterNo { get; set; }

        public decimal? HeatLossPcnt { get; set; }

        public decimal? OthCombDuty { get; set; }

        public decimal? OtherDuty { get; set; }

        public decimal? ProcessDuty { get; set; }
        
        public string ProcessFluid { get; set; }
        
        public string ProcessID { get; set; }
        
        public string Service { get; set; }

        public decimal? ShaftDuty { get; set; }

        public decimal? StackO2 { get; set; }

        public decimal? StackTemp { get; set; }

        public decimal? SteamDuty { get; set; }
        
        public char? SteamSuperHeated { get; set; }
        
        public int SubmissionID { get; set; }

        public decimal? ThroughputRpt { get; set; }
        
        public string ThroughputUOM { get; set; }
        
        public int UnitID { get; set; }
    }
}
