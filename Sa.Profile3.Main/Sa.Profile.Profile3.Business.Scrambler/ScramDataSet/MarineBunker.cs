using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class MarineBunker
    {
        public int BlendID { get; set; }
        
        public decimal? CrackedStock { get; set; }
        
        public decimal? Density { get; set; }
        
        public string Grade { get; set; }
        
        public decimal? KMT { get; set; }
        
        public decimal? PourPt { get; set; }
        
        public int SubmissionID { get; set; }
        
        public decimal? Sulfur { get; set; }
        
        public decimal? ViscCSAtTemp { get; set; }
        
        public decimal? ViscTemp { get; set; }
    }
}
