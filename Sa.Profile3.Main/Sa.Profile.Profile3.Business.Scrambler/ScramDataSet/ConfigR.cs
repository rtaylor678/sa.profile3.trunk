using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class ConfigR
    {
        public decimal? AvgSize { get; set; }
        
        public string ProcessID { get; set; }
        
        public string ProcessType { get; set; }
        
        public int SubmissionID { get; set; }

        public decimal? Throughput { get; set; }
        
        public int UnitID { get; set; }
    }
}
