using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{

    public class SteamSystem
    {
        public decimal? ActualPress { get; set; }
        
        public decimal? Calciner { get; set; }
        
        public decimal? ConsCombAirPreheat { get; set; }
        
        public decimal? ConsCondTurb { get; set; }
        
        public decimal? ConsDeaerators { get; set; }
        
        public decimal? ConsFlares { get; set; }
        
        public decimal? ConsOth { get; set; }
        
        public decimal? ConsOthHeaters { get; set; }
        
        public decimal? ConsPressControlHigh { get; set; }
        
        public decimal? ConsPressControlLow { get; set; }
        
        public decimal? ConsProcessCDU { get; set; }
        
        public decimal? ConsProcessCOK { get; set; }
        
        public decimal? ConsProcessFCC { get; set; }
        
        public decimal? ConsProcessOth { get; set; }
        
        public decimal? ConsReboil { get; set; }
        
        public decimal? ConsTopTurbHigh { get; set; }
        
        public decimal? ConsTopTurbLow { get; set; }
        
        public decimal? ConsTracingHeat { get; set; }
        
        public decimal? FCCCatCoolers { get; set; }
        
        public decimal? FCCStackGas { get; set; }
        
        public decimal? FiredBoiler { get; set; }
        
        public decimal? FiredProcessHeater { get; set; }
        
        public decimal? FluidCokerCOBoiler { get; set; }
        
        public decimal? FTCogen { get; set; }
        
        public decimal? H2PlantExport { get; set; }
        
        public decimal? NetPur { get; set; }
        
        public decimal? OthSource { get; set; }
        
        public string PressureRange { get; set; }
        
        public decimal? Pur { get; set; }
        
        public decimal? Sold { get; set; }
        
        public decimal? STProd { get; set; }
        
        public int SubmissionID { get; set; }
        
        public decimal? TotCons { get; set; }
        
        public decimal? TotSupply { get; set; }
        
        public decimal? WasteHeatCOK { get; set; }
        
        public decimal? WasteHeatFCC { get; set; }
        
        public decimal? WasteHeatOth { get; set; }
        
        public decimal? WasteHeatTCR { get; set; }
    }
}
