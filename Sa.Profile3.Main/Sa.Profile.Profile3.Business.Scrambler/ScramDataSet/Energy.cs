using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Energy
    {
        public decimal? Amount { get; set; }

        public decimal? Butane { get; set; }

        public decimal? Butylenes { get; set; }

        public decimal? C5Plus { get; set; }

        public decimal? CO { get; set; }

        public decimal? CO2 { get; set; }

        public string EnergyType { get; set; }

        public decimal? Ethane { get; set; }

        public decimal? Ethylene { get; set; }

        public decimal? GenEff { get; set; }

        public decimal? H2S { get; set; }

        public decimal? Hydrogen { get; set; }

        public decimal? Isobutane { get; set; }

        public decimal? MBTUOut { get; set; }

        public decimal? Methane { get; set; }

        public decimal? N2 { get; set; }

        public decimal? NH3 { get; set; }
        
        public char? OverrideCalcs { get; set; }

        public decimal? PriceLocal { get; set; }

        public decimal? Propane { get; set; }

        public decimal? Propylene { get; set; }

        public decimal? SO2 { get; set; }
        
        public int SubmissionID { get; set; }
        
        public string TransType { get; set; }
    }
}
