using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class MaintRout
    {
        public decimal? InServicePcnt { get; set; }
        
        public decimal? MaintDown { get; set; }
        
        public short? MaintNum { get; set; }
        
        public decimal? OthDown { get; set; }
        
        public decimal? OthDownEconomic { get; set; }
        
        public decimal? OthDownExternal { get; set; }
        
        public decimal? OthDownOffsiteUpsets { get; set; }
        
        public decimal? OthDownOther { get; set; }
        
        public decimal? OthDownUnitUpsets { get; set; }
        
        public short? OthNum { get; set; }
        
        public decimal? OthSlow { get; set; }
        
        public string ProcessID { get; set; }
        
        public decimal? RegDown { get; set; }
        
        public short? RegNum { get; set; }
        
        public decimal? RoutCostLocal { get; set; }
        
        public decimal? RoutCptlLocal { get; set; }
        
        public decimal? RoutExpLocal { get; set; }
        
        public decimal? RoutOvhdLocal { get; set; }
        
        public int SubmissionID { get; set; }
        
        public int UnitID { get; set; }
        
        public decimal? UtilPcnt { get; set; }
    }
}
