using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class LPG
    {
        public int BlendID { get; set; }
        
        public char? MolOrVol { get; set; }
        
        public int SubmissionID { get; set; }
        
        public decimal? VolC2Lt { get; set; }
        
        public decimal? VolC3 { get; set; }
        
        public decimal? VolC3ene { get; set; }
        
        public decimal? VolC4ene { get; set; }
        
        public decimal? VolC5Plus { get; set; }
        
        public decimal? VoliC4 { get; set; }
        
        public decimal? VolnC4 { get; set; }
    }
}
