using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class MExp
    {
        public decimal? ComplexCptl { get; set; }
        
        public decimal? ConstraintRemoval { get; set; }
        
        public decimal? Energy { get; set; }
        
        public decimal? MaintExp { get; set; }
        
        public decimal? MaintExpRout { get; set; }
        
        public decimal? MaintExpTA { get; set; }
        
        public decimal? MaintOvhd { get; set; }
        
        public decimal? MaintOvhdRout { get; set; }
        
        public decimal? MaintOvhdTA { get; set; }
        
        public decimal? NonMaintInvestExp { get; set; }
        
        public decimal? NonRefExcl { get; set; }
        
        public decimal? NonRegUnit { get; set; }
        
        public decimal? OthInvest { get; set; }
        
        public decimal? RegDiesel { get; set; }
        
        public decimal? RegExp { get; set; }
        
        public decimal? RegGaso { get; set; }
        
        public decimal? RegOth { get; set; }
        
        public decimal? RoutMaintCptl { get; set; }
        
        public decimal? SafetyOth { get; set; }
        
        public int? SubmissionID { get; set; }
        
        public decimal? TAMaintCptl { get; set; }
        
        public decimal? TotMaint { get; set; }
    }
}
