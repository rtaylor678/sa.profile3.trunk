﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using System.IO;
using System.Configuration;

using Sa.Profile.Profile3.Business.Scrambler;
using Sa.Profile.Profile3.Services.Library;

namespace Sa.Profile.Profile3.UI.ClientStudyManager
{
    public partial class ClientStudyForm : Form
    {
        private string _clientFilesSentPath;
        private string _kpiFilesReceivedPath;

        public ClientStudyForm()
        {
            InitializeComponent();
            try
            {
                SetUIScreen(Mode.OFF);
                string baseDir = ConfigurationManager.AppSettings["SolomonBaseDir"];
                if (baseDir == null)
                {
                    MessageBox.Show("SolomonBaseDir variable missing from App.config file." + System.Environment.NewLine + "Please create an entry for this variable with a pattern of: " + System.Environment.NewLine +  "<add key='SolomonBaseDir' value='C:\\SA' />" + System.Environment.NewLine + " with no trailing backslash before running application.","Error");
                    return;
                }
                if (Directory.Exists(baseDir) == false)
                {
                    MessageBox.Show("SolomonBaseDir: " + baseDir + " does not exist.  Creating directory...", "Config Directory");
                    Directory.CreateDirectory(baseDir);
             
                }
                _clientFilesSentPath = baseDir + "\\ClientFilesSent";
                if (Directory.Exists(_clientFilesSentPath) == false)
                {
                    Directory.CreateDirectory(_clientFilesSentPath);
                }
                _kpiFilesReceivedPath = baseDir + "\\KPIFilesReceived";
                if (Directory.Exists(_kpiFilesReceivedPath) == false)
                {
                    Directory.CreateDirectory(_kpiFilesReceivedPath);
                }
                SetUIScreen(Mode.ON);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void LoadRefineryDataButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadedFilePathLabel.Text = "";
                LoadedFileLabel.Text = "";
                string fileName;
                openFileDialog1.Filter = "Refinery_XML_File|*.xml|All Files (*.*)|*.*";
                DialogResult dr = openFileDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    fileName = openFileDialog1.FileName;
                }
                else
                {
                    MessageBox.Show("Invalid data file.", "Error");
                    return;
                }
                Business.Scrambler.NewDataSet testFile = new Business.Scrambler.NewDataSet();
                testFile.ReadXml(fileName);
                LoadedFilePathLabel.Text = fileName;
                LoadedFileLabel.Text = openFileDialog1.SafeFileName;
                MessageBox.Show("File loaded.", "Info");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }     
 
        private class ClientSubmitted
        {
            public string Status { get; set; }
            public string fileName { get; set; }
            public string fileNameAndPath { get; set; }
            public int clientSubmissionID { get; set; }
            public Sa.Profile.Profile3.Services.Library.PL plantWeb { get; set; }
        }

        private void SubmitStudyButton_Click(object sender, EventArgs e)
        {
            if (LoadedFilePathLabel.Text.Trim().Length == 0)
            {
                MessageBox.Show("No Data File Loaded!  Pleaase click the <Load Refinery Data File> Button before submitting data.", "Info");
                return;
            }
            Sa.Profile.Profile3.Business.PlantFactory.FuelsRefineryCompletePlant plant = null;
            Sa.Profile.Profile3.Business.Scrambler.ScramPrep sp = null;
            ClientSubmitted cli = null;
            try
            {
                cli = new ClientSubmitted();
                cli.fileNameAndPath =  LoadedFilePathLabel.Text.Trim();
                cli.fileName = LoadedFileLabel.Text.Trim();
                //load again to make sure has not moved or been deleted
                Business.Scrambler.NewDataSet clientFile = new Business.Scrambler.NewDataSet();
                clientFile.ReadXml(cli.fileNameAndPath);
                sp = new Business.Scrambler.ScramPrep();
                plant = sp.ClientToBus(clientFile);
                List<string> brokenRules;
                List<string> warnings;
                if (plant.ValidatePlant(out brokenRules, out warnings) == false)
                {
                    if (brokenRules.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder(Environment.NewLine);
                        foreach (string broken in brokenRules)
                        {
                            sb.Append(broken);
                            sb.Append(Environment.NewLine);
                        }
                        MessageBox.Show("Broken Rules Detected.  Please fix error then try again." + sb.ToString(),"Broken Rules");
                        return;
                    }
                    if (warnings.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder(Environment.NewLine);
                        foreach (string broken in warnings)
                        {
                            sb.Append(broken);
                            sb.Append(Environment.NewLine);
                        }
                        if (MessageBox.Show("Warnings Detected!  Ignore warnings and proceed to submit data?" + sb.ToString(), "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                    }
                }
                cli.clientSubmissionID = (int)plant.PlantSubmission.SubmissionID;
                SubmissionIDTextBox.Text = plant.PlantSubmission.SubmissionID.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }          
            Sa.Profile.Profile3.Services.Library.PL plantWeb = sp.BusToWCF(plant);
            cli.plantWeb = plantWeb;
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.MarqueeAnimationSpeed = 60;
            SubmitRefineryDataBackgroundWorker1.RunWorkerAsync(cli);
        }

        private void SubmitRefineryDataBackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            ClientSubmitted fromClient = (ClientSubmitted)e.Argument;
            e.Result = DoSubmitData(fromClient);
        }

        private ClientSubmitted DoSubmitData(ClientSubmitted fromClient)
        {
            stud.StudyWebServiceClient client = null;
            ClientSubmitted results = new ClientSubmitted();
            results.fileNameAndPath = fromClient.fileNameAndPath;
            results.fileName = fromClient.fileName;
            results.clientSubmissionID = fromClient.clientSubmissionID;
            try
            {
                using (client = new stud.StudyWebServiceClient())
                {
                    Sa.Profile.Profile3.Services.Library.RequestSt req = new Sa.Profile.Profile3.Services.Library.RequestSt();
                    req.PLA = fromClient.plantWeb;
                    Sa.Profile.Profile3.Services.Library.ResponseKs response = client.SubmitSt(req);
                    if (response.HasErrors)
                    {
                        results.Status = response.Status;
                    }
                    else
                    {
                        results.Status = "ok";
                    }
                }
            }
            catch (Exception ex)
            {
                results.Status = GetFaultDetails(ex);
            }
            finally
            {
                if (client != null)
                {
                    client.Abort();
                }
            }
            return results;
        }

        private void SubmitRefineryDataBackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Style = ProgressBarStyle.Blocks;
            progressBar1.MarqueeAnimationSpeed = 0;
            ClientSubmitted whatClientSent = (ClientSubmitted)e.Result;
            if (whatClientSent.Status != "ok")
            {
                ErrorForm ef = new ErrorForm(whatClientSent.Status);
                ef.Show();
            }
            else
            {
                MessageBox.Show("Client Data Successfully Sent.", "Info");
                try
                {
                    string clientFile = whatClientSent.clientSubmissionID.ToString() + DateTime.Now.ToString("_MM_dd_yyyy_hhmmss_") + whatClientSent.fileName;

                    File.Copy(whatClientSent.fileNameAndPath, _clientFilesSentPath + "\\" + clientFile);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                    return;
                }
                LoadedFilePathLabel.Text = ""; LoadedFileLabel.Text = "";
            }
        }

        private void RequestStudyResultsButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Temp down! Awaiting calc fix from Greg.");
            return;
            int clientSubmissionID;
            StatusLabel.Text = "Status:";
            try
            {
                if (SubmissionIDTextBox.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Invalid ClientSubmissionID!", "Error");
                    return;
                }
                clientSubmissionID = int.Parse(SubmissionIDTextBox.Text.Trim());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                return;
            }
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.MarqueeAnimationSpeed = 60;
            RequestKPIsBackgroundWorker1.RunWorkerAsync(clientSubmissionID);
        }

        private class KPIResults
        {
            public int ClientSubmissionID { get; set; }
            public KPIResults() { }
            public KPR KPIsReturned { get; set; }
            public string Status { get; set; }
        }

        private void RequestKPIsBackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int subID = (int)e.Argument;
            e.Result = DoGetKPIs(subID);
        }

        private KPIResults DoGetKPIs(int submissionID)
        {
            KPIResults calcedKPIs = new KPIResults();
            calcedKPIs.ClientSubmissionID = submissionID;
            calcedKPIs.Status = "ok";
            stud.StudyWebServiceClient client = null;
            try
            {
                Sa.Profile.Profile3.Services.Library.ResponseKs result = null;
                using (client = new stud.StudyWebServiceClient())
                {
                    Sa.Profile.Profile3.Services.Library.RequestSt req = new Sa.Profile.Profile3.Services.Library.RequestSt();
                    req.SubmissionID = int.Parse(SubmissionIDTextBox.Text.Trim());
                    result = client.RequestStRes(req);
                }
                calcedKPIs.KPIsReturned = result.ReturnKs;
                if (result.HasErrors)
                {
                    calcedKPIs.Status = result.Status;
                }
            }
            catch (Exception ex)
            {
                calcedKPIs.Status = ex.Message;// GetFaultDetails(ex);
            }
            finally
            {
                if (client != null)
                {
                    client.Abort();
                }
            }
            return calcedKPIs;
        }

        private void RequestKPIsBackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Style = ProgressBarStyle.Blocks;
            progressBar1.MarqueeAnimationSpeed = 0;
            KPIResults kpis = (KPIResults)e.Result;
            if (kpis.Status != "ok")
            {
                ErrorForm ef = new ErrorForm(kpis.Status);
                ef.Show();
            }
            else
            {
                string kpiFile = kpis.ClientSubmissionID.ToString() + DateTime.Now.ToString("_MM_dd_yyyy_hhmmss") + ".xml";
                Sa.Profile.Profile3.Common.XmlTools.SerializeToXML<KPR>(kpis.KPIsReturned, _kpiFilesReceivedPath + "\\" + kpiFile);
                MessageBox.Show("KPIs returned and saved as: " + kpiFile,"File Downloaded");
                System.Diagnostics.Process.Start("explorer.exe", _kpiFilesReceivedPath);
            }
        }

        private static string GetFaultDetails(Exception ex)
        {
            var faultContract = ex as FaultException<Sa.Profile.Profile3.Services.Library.StudyCalculationFault>;
            StringBuilder sb = new StringBuilder();
            if (faultContract != null)
            {                
                StudyCalculationFault salaryCalculationFault = faultContract.Detail;
                sb.Append("Fault contract detail: ");
                sb.Append(System.Environment.NewLine);
                sb.Append(String.Format("Fault ID: {0}", salaryCalculationFault.FaultID));
                sb.Append(System.Environment.NewLine);
                sb.Append(String.Format("Message: {0}", salaryCalculationFault.FaultMessage));              
            }  
            return sb.ToString();
        }
        
        private enum Mode { ON, OFF }

        private void SetUIScreen(Mode mode)
        {
            MainGroupBox.Enabled = (mode == Mode.ON);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            if (RequestKPIsBackgroundWorker1.IsBusy)
            {
                RequestKPIsBackgroundWorker1.CancelAsync();
                RequestKPIsBackgroundWorker1.Dispose();
                RequestKPIsBackgroundWorker1 = null;
                GC.Collect();
            }
            Application.Exit();
        }

        private void ViewFilesSubmittedButton_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", _clientFilesSentPath);
        }

        private void ViewKPIsReceivedButton_Click(object sender, EventArgs e)
        {

        }


    }
}
