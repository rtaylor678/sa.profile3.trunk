﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.Profile.Profile3.Common
{
    public class XmlTools
    {
        public static void SerializeToXML<T>(T t, string inFileName)
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(t.GetType());
            System.IO.TextWriter tw = new System.IO.StreamWriter(inFileName);
            ser.Serialize(tw, t);
            tw.Close();
            tw.Dispose();
        }
        public static T DeserializeFromXML<T>(string inFileName)
        {
            System.Xml.Serialization.XmlSerializer des = new System.Xml.Serialization.XmlSerializer(typeof(T));
            System.IO.TextReader tr = new System.IO.StreamReader(inFileName);
            T retVal = (T)des.Deserialize(tr);
            tr.Close();
            return retVal;
        }
    }
}
