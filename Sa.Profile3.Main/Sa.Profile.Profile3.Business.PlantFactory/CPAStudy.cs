using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Sa.Profile.Profile3.Business.PlantFactory
{
    
    public enum CPAStudy
    {
        NoClientData,
        ValidClientData,
        PierGroupData,
        RefineryRanking,
        NSA1,
        LubeData,
        CSNSA1,
        CustomAndWorldPierGroups,
        NSA2
    }
}
