using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Sa.Profile.Profile3.Business.PlantFactory
{
    
    public class CompletePlantMaker : PlantMaker
    {
        public override BasicPlant CreatePlant(string type)
        {
            BasicPlant plant = null;
            string str = type;
            switch (str)
            {
                case null:
                case "chem":
                case "coal":
                    return plant;
            }
            if (!(str == "fuels"))
            {
                return plant;
            }
            return new FuelsRefineryCompletePlant(new FuelsRefineryPlantUnitFactory(PlantLevel.COMPLETE));
        }
        
        ~CompletePlantMaker()
        {
        }
    }
}
