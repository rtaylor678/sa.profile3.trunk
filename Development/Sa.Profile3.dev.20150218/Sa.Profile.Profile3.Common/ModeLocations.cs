﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.Profile.Profile3.Common
{
    public class ModeLocations
    {
        public ModeLocations() { }
        public static string DEV_MODE_DIR
        {
            get { return "C:\\KNPC\\Xml\\dev\\"; }
        }
        public static string TEST_MODE_DIR
        {
            get { return "C:\\KNPC\\Xml\\test\\"; }
        }
        public static string TEST_MODE_BUILD_DIR
        {
            get { return "C:\\KNPC\\Xml\\test\\build\\"; }
        }
        public static string PROD_MODE_DIR
        {
            get { return "C:\\KNPC\\Xml\\prod\\"; }
        }
    }
}
