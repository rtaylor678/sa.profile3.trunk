﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sa.Profile.Profile3.Business.PlantFactory;
using Sa.Profile.Profile3.Business.PlantUnits;
using Sa.Profile.Profile3.Business.PlantStudyClient;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using System.Reflection;
using Ninject;
using Sa.Profile.Profile3.Repository.SA;

//this project is used for misc tests
namespace Sa.Profile.Profile3.UI.StudyManager
{
    public partial class Form1 : Form
    {
        IRepository _repos;
        public Form1(IRepository repIn)
        {
            InitializeComponent();
            _repos = repIn;
        }

        private void PlantButton_Click(object sender, EventArgs e)
        {
            FuelsRefineryCompletePlantStudy ps = DeserializeFromXML<FuelsRefineryCompletePlantStudy>(_repos.CurrentRepositoryParms.XMLFolder + "9.xml");
            FuelsRefineryCompletePlant pl=  (FuelsRefineryCompletePlant)ps.Plant;
            List<string> errors = null;
            List<string> warnings = null;
            bool val = pl.ValidatePlant(out errors, out warnings);       
        }

        public static void SerializeToXML<T>(T t, string inFileName)
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(t.GetType());
            System.IO.TextWriter tw = new System.IO.StreamWriter(inFileName);
            ser.Serialize(tw, t);
            tw.Close();
            tw.Dispose();
        }
        public static T DeserializeFromXML<T>(string inFileName)
        {
            System.Xml.Serialization.XmlSerializer des = new System.Xml.Serialization.XmlSerializer(typeof(T));
            System.IO.TextReader tr = new System.IO.StreamReader(inFileName);
            T retVal = (T)des.Deserialize(tr);
            tr.Close();
            return retVal;
        }

        public class MyPers
        {
            public MyPers() { }
            public List<Sa.Profile.Profile3.Business.Scrambler.ScramDataSet.Per> peop { get; set; }
        }

        private void XMLPrepButton2_Click(object sender, EventArgs e)
        {
            try
            {          
                Sa.Profile.Profile3.Business.Scrambler.ScramPrep sp = new Business.Scrambler.ScramPrep();
                Sa.Profile.Profile3.Services.Library.PL plRet = sp.PrimeScram("SubmissionFull.xml", null);
               FuelsRefineryCompletePlant back = sp.WCFToBus(plRet);
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetKPIsButton_Click(object sender, EventArgs e)
        {
            string subID = SubmissionIDTextBox.Text;
            if (subID.Length == 0)
            {
                MessageBox.Show("Invalid Submission ID");
                return;
            }
            Sa.Profile.Profile3.Business.PlantStudySA.FuelsRefineryCompletePlantStudySA sa = new Business.PlantStudySA.FuelsRefineryCompletePlantStudySA(_repos);
            FuelsRefineryCompletePlantStudy stud2 = sa.GetStudyResults(int.Parse(subID));
            MessageBox.Show("Results retrieved!");
        }

        private void CreateStudyButton_Click(object sender, EventArgs e)
        {
            Sa.Profile.Profile3.Business.PlantStudySA.FuelsRefineryCompletePlantStudySA sa = new Business.PlantStudySA.FuelsRefineryCompletePlantStudySA(_repos);  
            Sa.Profile.Profile3.Business.Scrambler.ScramPrep sp = new Business.Scrambler.ScramPrep();
            Sa.Profile.Profile3.Services.Library.PL plRet = sp.PrimeScram(_repos.CurrentRepositoryParms.XMLFolder+"SubmissionFull2.xml", null);
            FuelsRefineryCompletePlant back = sp.WCFToBus(plRet);
            FuelsRefineryCompletePlantStudy ps = new FuelsRefineryCompletePlantStudy(back);                     
            sa.DoCPAStudy(ps);
            MessageBox.Show("Study Created");
        }

        private void ScramblerButton_Click(object sender, EventArgs e)
        {
            //map checking code
            Sa.Profile.Profile3.DataAccess.MapCheckRepository mp = new DataAccess.MapCheckRepository();
            mp.ClearAllBeforeFields();
            mp.ClearAllAfterFields();
            Sa.Profile.Profile3.DataAccess.FieldIncrem.InitFieldIncrem(DataAccess.FieldIncremMode.LARGE);
            string err="";
            Sa.Profile.Profile3.Business.Scrambler.NewDataSet newDS = mp.PrimeClientDataSetWithIncremData(out err);
            if (err.Length > 0)
            {
                MessageBox.Show("Error:" + err);
                return;
            }

            bool ret = mp.PrimeBeforeWithClientDataSetData(newDS, out err);
            if (err.Length > 0)
            {
                MessageBox.Show("Error:" + err);
                return;
            }
            Sa.Profile.Profile3.Business.Scrambler.ScramPrep sp = new Business.Scrambler.ScramPrep();
            Sa.Profile.Profile3.Business.PlantFactory.FuelsRefineryCompletePlant cli = sp.ClientToBus(newDS);
            Sa.Profile.Profile3.Services.Library.PL plRet = sp.BusToWCF(cli);      
            FuelsRefineryCompletePlant back = sp.WCFToBus(plRet);
            mp.PrimeAfterWithBusinessObjectData(back, out err);
            if (err.Length > 0)
            {
                MessageBox.Show("Error:" + err);
                return;
            }
            MessageBox.Show("Done");
        }
    }
}
