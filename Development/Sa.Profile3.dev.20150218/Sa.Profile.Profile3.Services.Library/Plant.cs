﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
//using System.ServiceModel;
using System.Text;

namespace Sa.Profile.Profile3.Services.Library
{
    [DataContract]
    public class PL
    {
        [DataMember]
        public List<A> A_List { get; set; }
        [DataMember]
        public List<C> C_List { get; set; }
        [DataMember]
        public List<CB> CB_List { get; set; }
        [DataMember]
        public List<CD> CD_List { get; set; }
        [DataMember]
        public List<CR> CR_List { get; set; }
        [DataMember]
        public List<D> D_List { get; set; }
        [DataMember]
        public List<E> E_List { get; set; }
        [DataMember]
        public List<EG> EG_List { get; set; }
        [DataMember]
        public List<EL> EL_List { get; set; }
        [DataMember]
        public List<F> F_List { get; set; }
        [DataMember]
        public List<G> G_List { get; set; }
        [DataMember]
        public List<GM> GM_List { get; set; }
        [DataMember]
        public List<I> I_List { get; set; }
        [DataMember]
        public List<K> K_List { get; set; }
        [DataMember]
        public List<L> L_List { get; set; }
        [DataMember]
        public List<M> M_List { get; set; }
        [DataMember]
        public List<ME> ME_List { get; set; }
        [DataMember]
        public List<MI> MI_List { get; set; }
        [DataMember]
        public List<MR> MR_List { get; set; }
        [DataMember]
        public List<MT> MT_List { get; set; }
        [DataMember]
        public List<O> O_List { get; set; }
        [DataMember]
        public List<P> P_List { get; set; }
        [DataMember]
        public List<PD> PD_List { get; set; }
        [DataMember]
        public List<R> R_List { get; set; }
        [DataMember]
        public List<RP> RP_List { get; set; }
        [DataMember]
        public List<S> S_List { get; set; }
        [DataMember]
        public List<SB> SB_List { get; set; }
        [DataMember]
        public List<Y> Y_List { get; set; }

    }

    [DataContract]
    public class A {
        [DataMember]
        public decimal? A1 { get; set; }//A
        [DataMember]
        public decimal? A2 { get; set; }//A
        [DataMember]
        public string A3 { get; set; }//A
        [DataMember]
        public int? A4 { get; set; }//A  
    }
    [DataContract]
    public class C {
        [DataMember]
        public decimal? C1 { get; set; }//C
        [DataMember]
        public decimal? C2 { get; set; }//C
        [DataMember]
        public string C3 { get; set; }//C
        [DataMember]
        public decimal? C4 { get; set; }//C
        [DataMember]
        public decimal? C5 { get; set; }//C
        [DataMember]
        public decimal? C6 { get; set; }//C
        [DataMember]
        public decimal? C7 { get; set; }//C
        [DataMember]
        public decimal? C8 { get; set; }//C
        [DataMember]
        public decimal? C9 { get; set; }//C
        [DataMember]
        public decimal? C10 { get; set; }//C
        [DataMember]
        public decimal? C11 { get; set; }//C
        [DataMember]
        public string C12 { get; set; }//C
        [DataMember]
        public string C13 { get; set; }//C
        [DataMember]
        public string C14 { get; set; }//C
        [DataMember]
        public int? C15 { get; set; }//C
        [DataMember]
        public int? C16 { get; set; }//C    
    }
    [DataContract]
    public class CB {
        [DataMember]
        public decimal? CB1 { get; set; }//CB
        [DataMember]
        public decimal? CB2 { get; set; }//CB
        [DataMember]
        public decimal? CB3 { get; set; }//CB
        [DataMember]
        public string CB4 { get; set; }//CB
        [DataMember]
        public string CB5 { get; set; }//CB
        [DataMember]
        public int? CB6 { get; set; }//CB
        [DataMember]
        public int? CB7 { get; set; }//CB   
    }
    [DataContract]
    public class CD {
        [DataMember]
        public decimal? CD1 { get; set; }//CD
        [DataMember]
        public decimal? CD2 { get; set; }//CD
        [DataMember]
        public decimal? CD3 { get; set; }//CD
        [DataMember]
        public string CD4 { get; set; }//CD
        [DataMember]
        public string CD5 { get; set; }//CD
        [DataMember]
        public int? CD6 { get; set; }//CD
        [DataMember]
        public string CD7 { get; set; }//CD
        [DataMember]
        public int? CD8 { get; set; }//CD    
    }
    [DataContract]
    public class CR {
        [DataMember]public decimal? CR1{ get; set; }//CR
        [DataMember]public decimal? CR2{ get; set; }//CR
        [DataMember]public string CR3{ get; set; }//CR
        [DataMember]public string CR4{ get; set; }//CR
        [DataMember]public int? CR5{ get; set; }//CR
        [DataMember]public int? CR6{ get; set; }//CR
    }
    [DataContract]
    public class D {
        [DataMember]public decimal? D1{ get; set; }//D
        [DataMember]public decimal? D2{ get; set; }//D
        [DataMember]public decimal? D3{ get; set; }//D
        [DataMember]public decimal? D4{ get; set; }//D
        [DataMember]public decimal? D5{ get; set; }//D
        [DataMember]public decimal? D6{ get; set; }//D
        [DataMember]public decimal? D7{ get; set; }//D
        [DataMember]public decimal? D8{ get; set; }//D
        [DataMember]
        public decimal? D9 { get; set; }//D
        [DataMember]
        public string D10 { get; set; }//D
        [DataMember]
        public string D11 { get; set; }//D
        [DataMember]
        public string D12 { get; set; }//D
        [DataMember]
        public int? D13 { get; set; }//D
        [DataMember]
        public int? D14 { get; set; }//D    
    }
    [DataContract]
    public class E {
        [DataMember]
        public decimal? E1 { get; set; }//E
        [DataMember]
        public string E2 { get; set; }//E
        [DataMember]
        public int? E3 { get; set; }//E  
    }
    [DataContract]
    public class EG {
        [DataMember]
        public decimal? EG1 { get; set; }//EG
        [DataMember]
        public decimal? EG2 { get; set; }//EG
        [DataMember]
        public decimal? EG3 { get; set; }//EG
        [DataMember]
        public decimal? EG4 { get; set; }//EG
        [DataMember]
        public decimal? EG5 { get; set; }//EG
        [DataMember]
        public decimal? EG6 { get; set; }//EG
        [DataMember]
        public decimal? EG7 { get; set; }//EG
        [DataMember]
        public decimal? EG8 { get; set; }//EG
        [DataMember]
        public decimal? EG9 { get; set; }//EG
        [DataMember]
        public decimal? EG10 { get; set; }//EG
        [DataMember]
        public decimal? EG11 { get; set; }//EG
        [DataMember]
        public decimal? EG12 { get; set; }//EG
        [DataMember]
        public decimal? EG13 { get; set; }//EG
        [DataMember]
        public decimal? EG14 { get; set; }//EG
        [DataMember]
        public decimal? EG15 { get; set; }//EG
        [DataMember]
        public decimal? EG16 { get; set; }//EG
        [DataMember]
        public decimal? EG17 { get; set; }//EG
        [DataMember]
        public char? EG18 { get; set; }//EG
        [DataMember]
        public decimal? EG19 { get; set; }//EG
        [DataMember]
        public decimal? EG20 { get; set; }//EG
        [DataMember]
        public decimal? EG21 { get; set; }//EG
        [DataMember]
        public string EG22 { get; set; }//EG
        [DataMember]
        public string EG23 { get; set; }//EG
        [DataMember]
        public int? EG24 { get; set; }//EG    
    }
    [DataContract]
    public class F {
        [DataMember]
        public decimal? F1 { get; set; }//F
        [DataMember]
        public decimal? F2 { get; set; }//F
        [DataMember]
        public char? F3 { get; set; }//F
        [DataMember]
        public decimal? F4 { get; set; }//F
        [DataMember]
        public decimal? F5 { get; set; }//F
        [DataMember]
        public decimal? F6 { get; set; }//F
        [DataMember]
        public decimal? F7 { get; set; }//F
        [DataMember]
        public decimal? F8 { get; set; }//F
        [DataMember]
        public decimal? F9 { get; set; }//F
        [DataMember]
        public decimal? F10 { get; set; }//F
        [DataMember]
        public decimal? F11 { get; set; }//F
        [DataMember]
        public decimal? F12 { get; set; }//F
        [DataMember]
        public decimal? F13 { get; set; }//F
        [DataMember]
        public string F14 { get; set; }//F
        [DataMember]
        public decimal? F15 { get; set; }//F
        [DataMember]
        public string F16 { get; set; }//F
        [DataMember]
        public decimal? F17 { get; set; }//F
        [DataMember]
        public string F18 { get; set; }//F
        [DataMember]
        public string F19 { get; set; }//F
        [DataMember]
        public string F20 { get; set; }//F
        [DataMember]
        public string F21 { get; set; }//F
        [DataMember]
        public int? F22 { get; set; }//F
        [DataMember]
        public int? F23 { get; set; }//F
        [DataMember]
        public int? F24 { get; set; }//F   
    }
    [DataContract]
    public class G {
        [DataMember]
        public decimal? G1 { get; set; }//G
        [DataMember]
        public decimal? G2 { get; set; }//G
        [DataMember]
        public decimal? G3 { get; set; }//G
        [DataMember]
        public decimal? G4 { get; set; }//G
        [DataMember]
        public decimal? G5 { get; set; }//G
        [DataMember]
        public decimal? G6 { get; set; }//G
        [DataMember]
        public decimal? G7 { get; set; }//G
        [DataMember]
        public decimal? G8 { get; set; }//G
        [DataMember]
        public decimal? G9 { get; set; }//G
        [DataMember]
        public decimal? G10 { get; set; }//G
        [DataMember]
        public decimal? G11 { get; set; }//G
        [DataMember]
        public decimal? G12 { get; set; }//G
        [DataMember]
        public decimal? G13 { get; set; }//G
        [DataMember]
        public string G14 { get; set; }//G
        [DataMember]
        public string G15 { get; set; }//G
        [DataMember]
        public string G16 { get; set; }//G
        [DataMember]
        public int? G17 { get; set; }//G
        [DataMember]
        public int? G18 { get; set; }//G   
    }
    [DataContract]
    public class GM {
        [DataMember]
        public decimal? GM1 { get; set; }//GM
        [DataMember]
        public decimal? GM2 { get; set; }//GM
        [DataMember]
        public int? GM3 { get; set; }//GM   
    }
    [DataContract]
    public class I {
        [DataMember]
        public decimal? I1 { get; set; }//I
        [DataMember]
        public decimal? I2 { get; set; }//I
        [DataMember]
        public int? I3 { get; set; }//I
        [DataMember]
        public decimal? I4 { get; set; }//I
        [DataMember]
        public decimal? I5 { get; set; }//I
        [DataMember]
        public decimal? I6 { get; set; }//I
        [DataMember]
        public decimal? I7 { get; set; }//I
        [DataMember]
        public string I8 { get; set; }//I
        [DataMember]
        public int? I9 { get; set; }//I   
    }
    [DataContract]
    public class K {
        [DataMember]
        public decimal? K1 { get; set; }//K
        [DataMember]
        public decimal? K2 { get; set; }//K
        [DataMember]
        public decimal? K3 { get; set; }//K
        [DataMember]
        public string K4 { get; set; }//K
        [DataMember]
        public string K5 { get; set; }//K
        [DataMember]
        public int? K6 { get; set; }//K
        [DataMember]
        public int? K7 { get; set; }//K    
    }
    [DataContract]
    public class L {
        [DataMember]
        public decimal? L1 { get; set; }//L
        [DataMember]
        public decimal? L2 { get; set; }//L
        [DataMember]
        public decimal? L3 { get; set; }//L
        [DataMember]
        public decimal? L4 { get; set; }//L
        [DataMember]
        public decimal? L5 { get; set; }//L
        [DataMember]
        public decimal? L6 { get; set; }//L
        [DataMember]
        public decimal? L7 { get; set; }//L
        [DataMember]
        public char? L8 { get; set; }//L
        [DataMember]
        public int? L9 { get; set; }//L
        [DataMember]
        public int? L10 { get; set; }//L    
    }
    [DataContract]
    public class M {
        [DataMember]
        public decimal? M1 { get; set; }//M
        [DataMember]
        public decimal? M2 { get; set; }//M
        [DataMember]
        public decimal? M3 { get; set; }//M
        [DataMember]
        public decimal? M4 { get; set; }//M
        [DataMember]
        public decimal? M5 { get; set; }//M
        [DataMember]
        public decimal? M6 { get; set; }//M
        [DataMember]
        public decimal? M7 { get; set; }//M
        [DataMember]
        public string M8 { get; set; }//M
        [DataMember]
        public int? M9 { get; set; }//M
        [DataMember]
        public int? M10 { get; set; }//M    
    }
    [DataContract]
    public class ME {
        [DataMember]
        public decimal? ME1 { get; set; }//ME
        [DataMember]
        public decimal? ME2 { get; set; }//ME
        [DataMember]
        public decimal? ME3 { get; set; }//ME
        [DataMember]
        public decimal? ME4 { get; set; }//ME
        [DataMember]
        public decimal? ME5 { get; set; }//ME
        [DataMember]
        public decimal? ME6 { get; set; }//ME
        [DataMember]
        public decimal? ME7 { get; set; }//ME
        [DataMember]
        public decimal? ME8 { get; set; }//ME
        [DataMember]
        public decimal? ME9 { get; set; }//ME
        [DataMember]
        public decimal? ME10 { get; set; }//ME
        [DataMember]
        public decimal? ME11 { get; set; }//ME
        [DataMember]
        public decimal? ME12 { get; set; }//ME
        [DataMember]
        public decimal? ME13 { get; set; }//ME
        [DataMember]
        public decimal? ME14 { get; set; }//ME
        [DataMember]
        public decimal? ME15 { get; set; }//ME
        [DataMember]
        public decimal? ME16 { get; set; }//ME
        [DataMember]
        public decimal? ME17 { get; set; }//ME
        [DataMember]
        public decimal? ME18 { get; set; }//ME
        [DataMember]
        public decimal? ME19 { get; set; }//ME
        [DataMember]
        public decimal? ME20 { get; set; }//ME
        [DataMember]
        public decimal? ME21 { get; set; }//ME
        [DataMember]
        public int? ME22 { get; set; }//ME    
    }
    [DataContract]
    public class MI {
        [DataMember]
        public decimal? MI1 { get; set; }//MI
        [DataMember]
        public decimal? MI2 { get; set; }//MI
        [DataMember]
        public decimal? MI3 { get; set; }//MI
        [DataMember]
        public int? MI4 { get; set; }//MI   
    }
    [DataContract]
    public class MR {
        [DataMember]
        public decimal? MR1 { get; set; }//MR
        [DataMember]
        public decimal? MR2 { get; set; }//MR
        [DataMember]
        public decimal? MR3 { get; set; }//MR
        [DataMember]
        public decimal? MR4 { get; set; }//MR
        [DataMember]
        public decimal? MR5 { get; set; }//MR
        [DataMember]
        public decimal? MR6 { get; set; }//MR
        [DataMember]
        public decimal? MR7 { get; set; }//MR
        [DataMember]
        public decimal? MR8 { get; set; }//MR
        [DataMember]
        public decimal? MR9 { get; set; }//MR
        [DataMember]
        public decimal? MR10 { get; set; }//MR
        [DataMember]
        public decimal? MR11 { get; set; }//MR
        [DataMember]
        public decimal? MR12 { get; set; }//MR
        [DataMember]
        public decimal? MR13 { get; set; }//MR
        [DataMember]
        public int? MR14 { get; set; }//MR
        [DataMember]
        public decimal? MR15 { get; set; }//MR
        [DataMember]
        public int? MR16 { get; set; }//MR
        [DataMember]
        public decimal? MR17 { get; set; }//MR
        [DataMember]
        public int? MR18 { get; set; }//MR
        [DataMember]
        public string MR19 { get; set; }//MR
        [DataMember]
        public int? MR20 { get; set; }//MR
        [DataMember]
        public int? MR21 { get; set; }//MR    
    }
    [DataContract]
    public class MT {
        [DataMember]
        public int? MT1 { get; set; }//MT
        [DataMember]
        public DateTime? MT2 { get; set; }//MT
        [DataMember]
        public decimal? MT3 { get; set; }//MT
        [DataMember]
        public decimal? MT4 { get; set; }//MT
        [DataMember]
        public decimal? MT5 { get; set; }//MT
        [DataMember]
        public decimal? MT6 { get; set; }//MT
        [DataMember]
        public decimal? MT7 { get; set; }//MT
        [DataMember]
        public decimal? MT8 { get; set; }//MT
        [DataMember]
        public decimal? MT9 { get; set; }//MT
        [DataMember]
        public decimal? MT10 { get; set; }//MT
        [DataMember]
        public decimal? MT11 { get; set; }//MT
        [DataMember]
        public decimal? MT12 { get; set; }//MT
        [DataMember]
        public decimal? MT13 { get; set; }//MT
        [DataMember]
        public decimal? MT14 { get; set; }//MT
        [DataMember]
        public DateTime? MT15 { get; set; }//MT
        [DataMember]
        public string MT16 { get; set; }//MT
        [DataMember]
        public int? MT17 { get; set; }//MT
        [DataMember]
        public int? MT18 { get; set; }//MT   
    }
    [DataContract]
    public class O {
        [DataMember]
        public string O1 { get; set; }//O
        [DataMember]
        public decimal? O2 { get; set; }//O
        [DataMember]
        public string O3 { get; set; }//O
        [DataMember]
        public int? O4 { get; set; }//O   
    }
    [DataContract]
    public class P
    {
        [DataMember]
        public decimal? P1 { get; set; }//P
        [DataMember]
        public decimal? P2 { get; set; }//P
        [DataMember]
        public decimal? P3 { get; set; }//P
        [DataMember]
        public decimal? P4 { get; set; }//P
        [DataMember]
        public decimal? P5 { get; set; }//P
        [DataMember]
        public decimal? P6 { get; set; }//P
        [DataMember]
        public decimal? P7 { get; set; }//P
        [DataMember]
        public string P8 { get; set; }//P
        [DataMember]
        public int? P9 { get; set; }//P    
    }
        [DataContract]
        public class PD
        {
            [DataMember]
            public string PD1 { get; set; }//PD
            [DataMember]
            public DateTime? PD2 { get; set; }//PD
            [DataMember]
            public string PD3 { get; set; }//PD
            [DataMember]
            public decimal? PD4 { get; set; }//PD
            [DataMember]
            public string PD5 { get; set; }//PD
            [DataMember]
            public int? PD6 { get; set; }//PD
            [DataMember]
            public int? PD7 { get; set; }//PD    
        }
        [DataContract]
        public class R
        {
            [DataMember]
            public decimal? R1 { get; set; }//R
            [DataMember]
            public decimal? R2 { get; set; }//R
            [DataMember]
            public decimal? R3 { get; set; }//R
            [DataMember]
            public decimal? R4 { get; set; }//R
            [DataMember]
            public decimal? R5 { get; set; }//R
            [DataMember]
            public decimal? R6 { get; set; }//R
            [DataMember]
            public string R7 { get; set; }//R
            [DataMember]
            public int? R8 { get; set; }//R
            [DataMember]
            public int? R9 { get; set; }//R    
        }
        [DataContract]
        public class RP
        {
            [DataMember]
            public decimal? RP1 { get; set; }//RP
            [DataMember]
            public decimal? RP2 { get; set; }//RP
            [DataMember]
            public decimal? RP3 { get; set; }//RP
            [DataMember]
            public decimal? RP4 { get; set; }//RP
            [DataMember]
            public string RP5 { get; set; }//RP
            [DataMember]
            public int? RP6 { get; set; }//RP    
        }
        [DataContract]
        public class S
        {

            [DataMember]
            public decimal? S1 { get; set; }//S
            [DataMember]
            public decimal? S2 { get; set; }//S
            [DataMember]
            public decimal? S3 { get; set; }//S
            [DataMember]
            public decimal? S4 { get; set; }//S
            [DataMember]
            public decimal? S5 { get; set; }//S
            [DataMember]
            public decimal? S6 { get; set; }//S
            [DataMember]
            public decimal? S7 { get; set; }//S
            [DataMember]
            public decimal? S8 { get; set; }//S
            [DataMember]
            public decimal? S9 { get; set; }//S
            [DataMember]
            public decimal? S10 { get; set; }//S
            [DataMember]
            public decimal? S11 { get; set; }//S
            [DataMember]
            public decimal? S12 { get; set; }//S
            [DataMember]
            public decimal? S13 { get; set; }//S
            [DataMember]
            public decimal? S14 { get; set; }//S
            [DataMember]
            public decimal? S15 { get; set; }//S
            [DataMember]
            public decimal? S16 { get; set; }//S
            [DataMember]
            public decimal? S17 { get; set; }//S
            [DataMember]
            public decimal? S18 { get; set; }//S
            [DataMember]
            public decimal? S19 { get; set; }//S
            [DataMember]
            public decimal? S20 { get; set; }//S
            [DataMember]
            public decimal? S21 { get; set; }//S
            [DataMember]
            public decimal? S22 { get; set; }//S
            [DataMember]
            public decimal? S23 { get; set; }//S
            [DataMember]
            public decimal? S24 { get; set; }//S
            [DataMember]
            public decimal? S25 { get; set; }//S
            [DataMember]
            public decimal? S26 { get; set; }//S
            [DataMember]
            public decimal? S27 { get; set; }//S
            [DataMember]
            public decimal? S28 { get; set; }//S
            [DataMember]
            public decimal? S29 { get; set; }//S
            [DataMember]
            public decimal? S30 { get; set; }//S
            [DataMember]
            public decimal? S31 { get; set; }//S
            [DataMember]
            public decimal? S32 { get; set; }//S
            [DataMember]
            public decimal? S33 { get; set; }//S
            [DataMember]
            public decimal? S34 { get; set; }//S
            [DataMember]
            public decimal? S35 { get; set; }//S
            [DataMember]
            public decimal? S36 { get; set; }//S
            [DataMember]
            public string S37 { get; set; }//S
            [DataMember]
            public int? S38 { get; set; }//S    
        }
        [DataContract]
        public class SB
        {
            [DataMember]
            public string SB1 { get; set; }//SB
            [DataMember]
            public string SB2 { get; set; }//SB
            [DataMember]
            public DateTime? SB3 { get; set; }//SB
            [DataMember]
            public string SB4 { get; set; }//SB
            [DataMember]
            public int? SB5 { get; set; }//SB    
        }
        [DataContract]
        public class Y
        {
            [DataMember]
            public decimal? Y1 { get; set; }//Y
            [DataMember]
            public decimal? Y2 { get; set; }//Y
            [DataMember]
            public decimal? Y3 { get; set; }//Y
            [DataMember]
            public string Y4 { get; set; }//Y
            [DataMember]
            public string Y5 { get; set; }//Y
            [DataMember]
            public string Y6 { get; set; }//Y
            [DataMember]
            public string Y7 { get; set; }//Y
            [DataMember]
            public int? Y8 { get; set; }//Y
        }
        [DataContract]
        public class EL
        {
            [DataMember]
            public decimal? EL1 { get; set; }//EL
            [DataMember]
            public decimal? EL2 { get; set; }//EL
            [DataMember]
            public decimal? EL3 { get; set; }//EL
            [DataMember]
            public string EL4 { get; set; }//EL
            [DataMember]
            public string EL5 { get; set; }//EL
            [DataMember]
            public int? EL6 { get; set; }//EL
        }

    }

