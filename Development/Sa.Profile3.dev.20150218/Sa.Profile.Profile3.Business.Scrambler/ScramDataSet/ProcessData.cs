using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    public class ProcessData
    {
        public string Property { get; set; }
        
        public DateTime? RptDVal { get; set; }
        
        public float? RptNVal { get; set; }
        
        public string RptTVal { get; set; }
        
        public int SubmissionID { get; set; }
        
        public int UnitID { get; set; }
        
        public string UOM { get; set; }
    }
}
