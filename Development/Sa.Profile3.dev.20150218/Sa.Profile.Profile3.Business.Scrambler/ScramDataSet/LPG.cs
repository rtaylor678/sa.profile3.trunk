using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class LPG
    {
        public int BlendID { get; set; }
        
        public char? MolOrVol { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? VolC2Lt { get; set; }
        
        public float? VolC3 { get; set; }
        
        public float? VolC3ene { get; set; }
        
        public float? VolC4ene { get; set; }
        
        public float? VolC5Plus { get; set; }
        
        public float? VoliC4 { get; set; }
        
        public float? VolnC4 { get; set; }
    }
}
