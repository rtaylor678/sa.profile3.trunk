using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class RPFResid
    {
        public float? Density { get; set; }
        
        public string EnergyType { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? Sulfur { get; set; }
        
        public float? ViscCSAtTemp { get; set; }
        
        public float? ViscTemp { get; set; }
    }
}
