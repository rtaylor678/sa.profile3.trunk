using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    public class GeneralMisc
    {
        public float? FlareLossMT { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? TotLossMT { get; set; }
    }
}
