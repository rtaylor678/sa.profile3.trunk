using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{

    public class SteamSystem
    {
        public float? ActualPress { get; set; }
        
        public float? Calciner { get; set; }
        
        public float? ConsCombAirPreheat { get; set; }
        
        public float? ConsCondTurb { get; set; }
        
        public float? ConsDeaerators { get; set; }
        
        public float? ConsFlares { get; set; }
        
        public float? ConsOth { get; set; }
        
        public float? ConsOthHeaters { get; set; }
        
        public float? ConsPressControlHigh { get; set; }
        
        public float? ConsPressControlLow { get; set; }
        
        public float? ConsProcessCDU { get; set; }
        
        public float? ConsProcessCOK { get; set; }
        
        public float? ConsProcessFCC { get; set; }
        
        public float? ConsProcessOth { get; set; }
        
        public float? ConsReboil { get; set; }
        
        public float? ConsTopTurbHigh { get; set; }
        
        public float? ConsTopTurbLow { get; set; }
        
        public float? ConsTracingHeat { get; set; }
        
        public float? FCCCatCoolers { get; set; }
        
        public float? FCCStackGas { get; set; }
        
        public float? FiredBoiler { get; set; }
        
        public float? FiredProcessHeater { get; set; }
        
        public float? FluidCokerCOBoiler { get; set; }
        
        public float? FTCogen { get; set; }
        
        public float? H2PlantExport { get; set; }
        
        public float? NetPur { get; set; }
        
        public float? OthSource { get; set; }
        
        public string PressureRange { get; set; }
        
        public float? Pur { get; set; }
        
        public float? Sold { get; set; }
        
        public float? STProd { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? TotCons { get; set; }
        
        public float? TotSupply { get; set; }
        
        public float? WasteHeatCOK { get; set; }
        
        public float? WasteHeatFCC { get; set; }
        
        public float? WasteHeatOth { get; set; }
        
        public float? WasteHeatTCR { get; set; }
    }
}
