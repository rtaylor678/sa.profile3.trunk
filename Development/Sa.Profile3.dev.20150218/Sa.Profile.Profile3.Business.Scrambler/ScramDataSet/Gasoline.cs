using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sa.Profile.Profile3.Business.Scrambler.ScramDataSet
{
    
    public class Gasoline
    {
        public int BlendID { get; set; }
        
        public float? Density { get; set; }
        
        public float? ETBE { get; set; }
        
        public float? Ethanol { get; set; }
        
        public string Grade { get; set; }
        
        public float? KMT { get; set; }
        
        public float? Lead { get; set; }
        
        public string Market { get; set; }
        
        public float? MON { get; set; }
        
        public float? MTBE { get; set; }
        
        public float? OthOxygen { get; set; }
        
        public float? Oxygen { get; set; }
        
        public float? RON { get; set; }
        
        public float? RVP { get; set; }
        
        public int SubmissionID { get; set; }
        
        public float? Sulfur { get; set; }
        
        public float? TAME { get; set; }
        
        public string Type { get; set; }
    }
}
